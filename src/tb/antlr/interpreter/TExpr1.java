// $ANTLR 3.4 /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g 2021-03-12 14:18:04

package tb.antlr.interpreter;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class TExpr1 extends MyTreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ID", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "POW", "RP", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ID=5;
    public static final int INT=6;
    public static final int LP=7;
    public static final int MINUS=8;
    public static final int MUL=9;
    public static final int NL=10;
    public static final int PLUS=11;
    public static final int PODST=12;
    public static final int POW=13;
    public static final int RP=14;
    public static final int VAR=15;
    public static final int WS=16;

    // delegates
    public MyTreeParser[] getDelegates() {
        return new MyTreeParser[] {};
    }

    // delegators


    public TExpr1(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public TExpr1(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return TExpr1.tokenNames; }
    public String getGrammarFileName() { return "/home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g"; }



    // $ANTLR start "prog"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:1: prog : (e= expr | deklaracja )* ;
    public final void prog() throws RecognitionException {
        TExpr1.expr_return e =null;


        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:9: ( (e= expr | deklaracja )* )
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:11: (e= expr | deklaracja )*
            {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:11: (e= expr | deklaracja )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= DIV && LA1_0 <= INT)||(LA1_0 >= MINUS && LA1_0 <= MUL)||LA1_0==PLUS||LA1_0==POW) ) {
                    alt1=1;
                }
                else if ( (LA1_0==PODST||LA1_0==VAR) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:12: e= expr
            	    {
            	    pushFollow(FOLLOW_expr_in_prog51);
            	    e=expr();

            	    state._fsp--;


            	    drukuj ((e!=null?(input.getTokenStream().toString(input.getTreeAdaptor().getTokenStartIndex(e.start),input.getTreeAdaptor().getTokenStopIndex(e.start))):null) + " = " + (e!=null?e.out:null).toString());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:69: deklaracja
            	    {
            	    pushFollow(FOLLOW_deklaracja_in_prog57);
            	    deklaracja();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "prog"


    public static class expr_return extends TreeRuleReturnScope {
        public Integer out;
    };


    // $ANTLR start "expr"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:16:1: expr returns [Integer out] : ( ^( PLUS e1= expr e2= expr ) | ^( MINUS e1= expr e2= expr ) | ^( MUL e1= expr e2= expr ) | ^( DIV e1= expr e2= expr ) | ^( POW e1= expr e2= expr ) | INT | ID );
    public final TExpr1.expr_return expr() throws RecognitionException {
        TExpr1.expr_return retval = new TExpr1.expr_return();
        retval.start = input.LT(1);


        CommonTree INT1=null;
        CommonTree ID2=null;
        TExpr1.expr_return e1 =null;

        TExpr1.expr_return e2 =null;


        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:17:8: ( ^( PLUS e1= expr e2= expr ) | ^( MINUS e1= expr e2= expr ) | ^( MUL e1= expr e2= expr ) | ^( DIV e1= expr e2= expr ) | ^( POW e1= expr e2= expr ) | INT | ID )
            int alt2=7;
            switch ( input.LA(1) ) {
            case PLUS:
                {
                alt2=1;
                }
                break;
            case MINUS:
                {
                alt2=2;
                }
                break;
            case MUL:
                {
                alt2=3;
                }
                break;
            case DIV:
                {
                alt2=4;
                }
                break;
            case POW:
                {
                alt2=5;
                }
                break;
            case INT:
                {
                alt2=6;
                }
                break;
            case ID:
                {
                alt2=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:17:10: ^( PLUS e1= expr e2= expr )
                    {
                    match(input,PLUS,FOLLOW_PLUS_in_expr81); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr86);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr90);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = dodaj((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 2 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:18:11: ^( MINUS e1= expr e2= expr )
                    {
                    match(input,MINUS,FOLLOW_MINUS_in_expr106); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr110);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr114);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = odejmij((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 3 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:19:11: ^( MUL e1= expr e2= expr )
                    {
                    match(input,MUL,FOLLOW_MUL_in_expr130); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr136);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr140);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = pomnoz((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 4 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:20:11: ^( DIV e1= expr e2= expr )
                    {
                    match(input,DIV,FOLLOW_DIV_in_expr156); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr162);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr166);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = podziel((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 5 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:21:11: ^( POW e1= expr e2= expr )
                    {
                    match(input,POW,FOLLOW_POW_in_expr182); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr188);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr192);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = poteguj((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 6 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:22:11: INT
                    {
                    INT1=(CommonTree)match(input,INT,FOLLOW_INT_in_expr207); 

                    retval.out = getInt((INT1!=null?INT1.getText():null));

                    }
                    break;
                case 7 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:23:11: ID
                    {
                    ID2=(CommonTree)match(input,ID,FOLLOW_ID_in_expr242); 

                    retval.out = pobierzZmienna((ID2!=null?ID2.getText():null));

                    }
                    break;

            }
        }
        catch (Exception e) {
            drukuj("Wyjątek: ");
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"



    // $ANTLR start "deklaracja"
    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:27:1: deklaracja : ( ^( VAR i1= ID ) | ^( PODST i1= ID e2= expr ) );
    public final void deklaracja() throws RecognitionException {
        CommonTree i1=null;
        TExpr1.expr_return e2 =null;


        try {
            // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:28:9: ( ^( VAR i1= ID ) | ^( PODST i1= ID e2= expr ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==VAR) ) {
                alt3=1;
            }
            else if ( (LA3_0==PODST) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:28:11: ^( VAR i1= ID )
                    {
                    match(input,VAR,FOLLOW_VAR_in_deklaracja314); 

                    match(input, Token.DOWN, null); 
                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_deklaracja318); 

                    match(input, Token.UP, null); 


                    stworzZmienna((i1!=null?i1.getText():null));

                    }
                    break;
                case 2 :
                    // /home/student/Pulpit/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:29:11: ^( PODST i1= ID e2= expr )
                    {
                    match(input,PODST,FOLLOW_PODST_in_deklaracja334); 

                    match(input, Token.DOWN, null); 
                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_deklaracja338); 

                    pushFollow(FOLLOW_expr_in_deklaracja344);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    ustawZmienna((i1!=null?i1.getText():null), (e2!=null?e2.out:null)); drukuj((i1!=null?i1.getText():null) + " = " + (e2!=null?e2.out:null).toString());

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "deklaracja"

    // Delegated rules


 

    public static final BitSet FOLLOW_expr_in_prog51 = new BitSet(new long[]{0x000000000000BB72L});
    public static final BitSet FOLLOW_deklaracja_in_prog57 = new BitSet(new long[]{0x000000000000BB72L});
    public static final BitSet FOLLOW_PLUS_in_expr81 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr86 = new BitSet(new long[]{0x0000000000002B70L});
    public static final BitSet FOLLOW_expr_in_expr90 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_in_expr106 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr110 = new BitSet(new long[]{0x0000000000002B70L});
    public static final BitSet FOLLOW_expr_in_expr114 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MUL_in_expr130 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr136 = new BitSet(new long[]{0x0000000000002B70L});
    public static final BitSet FOLLOW_expr_in_expr140 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DIV_in_expr156 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr162 = new BitSet(new long[]{0x0000000000002B70L});
    public static final BitSet FOLLOW_expr_in_expr166 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_POW_in_expr182 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr188 = new BitSet(new long[]{0x0000000000002B70L});
    public static final BitSet FOLLOW_expr_in_expr192 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_INT_in_expr207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_expr242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_deklaracja314 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_deklaracja318 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PODST_in_deklaracja334 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_deklaracja338 = new BitSet(new long[]{0x0000000000002B70L});
    public static final BitSet FOLLOW_expr_in_deklaracja344 = new BitSet(new long[]{0x0000000000000008L});

}